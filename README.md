# Vegan ou pas ?

## Requirements

Un fichier ```requirements.txt``` est présent à la racine. Depuis la racine, installez les dépendances avec ```pip install -r requirements.txt```.

## Start

Lancez ```main.py``` avec la commande ```python3 main.py```.

## Requêtes

Rendez-vous sur l'adresse indiquée (http://0.0.0.0:8000 généralement). Pour obtenir le résultat d'un produit, la structure est la suivante :
```{url}/isVegan/{id_produit}```.