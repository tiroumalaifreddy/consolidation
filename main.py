import uvicorn
import requests
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def product_data():
    return {"Hello" : "yes"}


def isVegan(requestAsJson):
    ingredients = requestAsJson["product"]["ingredients"]
    vegan = 0
    maybe = 0
    for ingredient in ingredients:
        if 'vegan' in ingredient:
            if ingredient['vegan'] == 'no':
                vegan = 1
            if ingredient['vegan'] == 'maybe':
                maybe = 1
    if vegan == 1:
        return False
    elif maybe == 1:
        return False
    else:
        return True


@app.get("/isVegan/{query}")
async def isitvegan(query : str):
    return {"isVegan" : isVegan(requests.get("https://world.openfoodfacts.org/api/v0/product/" + query +".json").json())}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)